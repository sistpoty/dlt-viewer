#!/usr/bin/make -f

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

# Ensure texlive respects SOURCE_DATE_EPOCH for reproducible
# timestamps
export FORCE_SOURCE_DATE=1

doc/dlt_viewer_user_manual.pdf: doc/dlt_viewer_user_manual.tex
	cd doc && \
		pdflatex $(^F) && \
		pdflatex $(^F) && \
		cd ..
	$(RM) doc/dlt_viewer_user_manual.aux \
		doc/dlt_viewer_user_manual.log \
		doc/dlt_viewer_user_manual.out \
		doc/dlt_viewer_user_manual.toc

doc/dlt_viewer_plugins_programming_guide.pdf: doc/dlt_viewer_plugins_programming_guide.txt
	cd doc && sh convert.sh && cd ..

%:
	dh $@

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test
	./obj-*/qdlt/tests/test_dltmessagematcher
	./obj-*/qdlt/tests/test_dltoptmanager
endif

override_dh_auto_configure:
	dh_auto_configure -- \
	      -DDLT_USE_STANDARD_INSTALLATION_LOCATION=ON \
	      -DDLT_USE_QT_RPATH=OFF \
	      -DDLT_PARSER=ON \
	      -DDLT_DUMMY_PLUGINS=ON \
	      -DDLT_INSTALL_SDK=OFF

override_dh_auto_install: doc/dlt_viewer_user_manual.pdf doc/dlt_viewer_plugins_programming_guide.pdf
	dh_auto_install

override_dh_clean:
	$(RM) doc/dlt_viewer_plugins_programming_guide.pdf \
		doc/dlt_viewer_user_manual.pdf \
		debian/doc-stamp
	dh_clean
